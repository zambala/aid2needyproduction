//
//  DonarDonationDetailsViewController.m
//  Aid2Needy
//
//  Created by Zenwise Technologies Private Limited on 02/01/19.
//  Copyright © 2019 Zenwise Technologies Private Limited. All rights reserved.
//

#import "DonarDonationDetailsViewController.h"
#import "DonarContactViewController.h"

@interface DonarDonationDetailsViewController ()

@end

@implementation DonarDonationDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectProductButton.layer.borderWidth = 1.0f;
    self.selectProductButton.layer.borderColor = [UIColor colorWithRed:(11.0/255.0) green:(72.0/255.0) blue:(107.0/255.0) alpha:1.0f].CGColor;
    
    self.expiryDateButton.layer.borderWidth = 1.0f;
    self.expiryDateButton.layer.borderColor = [UIColor colorWithRed:(11.0/255.0) green:(72.0/255.0) blue:(107.0/255.0) alpha:1.0f].CGColor;
    
    self.medicineTypeButton.layer.borderWidth = 1.0f;
    self.medicineTypeButton.layer.borderColor = [UIColor colorWithRed:(11.0/255.0) green:(72.0/255.0) blue:(107.0/255.0) alpha:1.0f].CGColor;
    
    self.continueButton.layer.cornerRadius = 5.0f;
    
    self.pickerView.hidden=YES;
    self.datePickerView.hidden = YES;
    
    
    [self.datePickerView setDatePickerMode:UIDatePickerModeDate];

    
    [self.selectProductButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.expiryDateButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.medicineTypeButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.continueButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.datePickerView addTarget:self action:@selector(onDatePickerTap:) forControlEvents:UIControlEventValueChanged];
    
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.selectProductButton)
    {
        self.pickerView.hidden=NO;
        self.datePickerView.hidden=YES;
        self.pickerView.delegate = self;
        self.pickerView.dataSource = self;
        [self.pickerView reloadAllComponents];
        UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDoneButtonTap)];
        UIToolbar* toolbar= [[UIToolbar alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-162,self.view.frame.size.width,50)];
        NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton,nil];
        [toolbar setItems:toolbarItems];
        [self.pickerView addSubview:toolbar];
    }else if (sender == self.expiryDateButton)
    {
        self.pickerView.hidden=YES;
        self.datePickerView.hidden=NO;
    }else if (sender == self.medicineTypeButton)
    {
        self.pickerView.hidden=NO;
        self.datePickerView.hidden=YES;
        self.pickerView.delegate = self;
        self.pickerView.dataSource = self;
        [self.pickerView reloadAllComponents];
    }else if (sender == self.continueButton)
    {
        DonarContactViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"DonarContactViewController"];
        [self.navigationController pushViewController:view animated:YES];
    }
}


-(void)onDatePickerTap
{
    self.datePickerView.hidden=YES;
}

- (void)onDatePickerTap:(UIDatePicker *)datePicker
{
    //self.expiryDateButton.titleLabel.text = [self.dateFormatter stringFromDate:datePicker.date];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"Choice-%ld",(long)row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    self.pickerView.hidden = YES;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
