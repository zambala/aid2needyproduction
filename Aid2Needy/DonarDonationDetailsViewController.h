//
//  DonarDonationDetailsViewController.h
//  Aid2Needy
//
//  Created by Zenwise Technologies Private Limited on 02/01/19.
//  Copyright © 2019 Zenwise Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DonarDonationDetailsViewController : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *manufacturerTF;
@property (weak, nonatomic) IBOutlet UITextField *medicineNameTF;
@property (weak, nonatomic) IBOutlet UITextField *quantityTF;
@property (weak, nonatomic) IBOutlet UIButton *selectProductButton;
@property (weak, nonatomic) IBOutlet UIButton *expiryDateButton;
@property (weak, nonatomic) IBOutlet UIButton *medicineTypeButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerView;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;


@end

NS_ASSUME_NONNULL_END
