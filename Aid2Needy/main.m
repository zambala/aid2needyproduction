//
//  main.m
//  Aid2Needy
//
//  Created by Zenwise Technologies Private Limited on 20/12/18.
//  Copyright © 2018 Zenwise Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
