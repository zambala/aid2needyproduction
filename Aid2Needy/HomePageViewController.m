//
//  HomePageViewController.m
//  Aid2Needy
//
//  Created by Zenwise Technologies Private Limited on 02/01/19.
//  Copyright © 2019 Zenwise Technologies Private Limited. All rights reserved.
//

#import "HomePageViewController.h"
#import "DonarDonationDetailsViewController.h"
#import "VolunteerLoginViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface HomePageViewController ()
{
    NSString * selectedUser;
}

@end

@implementation HomePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.proceedButton.layer.cornerRadius = 5.0f;
    self.backView.layer.cornerRadius=5.0f;
    self.backView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.16f] CGColor];
    self.backView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    self.backView.layer.shadowOpacity = 1.0f;
    self.backView.layer.shadowRadius = 3.0f;
    self.backView.layer.masksToBounds = NO;
    [self.donarButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.volunteerButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.proceedButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.proceedButton)
    {
        if(selectedUser.length>0)
        {
            if([selectedUser isEqualToString:@"donar"])
            {
        DonarDonationDetailsViewController * view = [self.storyboard instantiateViewControllerWithIdentifier:@"DonarDonationDetailsViewController"];
        [self.navigationController pushViewController:view animated:YES];
            }else if ([selectedUser isEqualToString:@"volunteer"])
            {
                VolunteerLoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"VolunteerLoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
            }
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"" message:@"Please choose one of the options mentioned above to continue." preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:^{
                    
                }];
                
                UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                }];
                
                [alert addAction:okAction];
            });
        }
        
    }else if (sender == self.donarButton)
    {
        self.donarButton.layer.borderWidth = 2.0f;
        self.donarButton.layer.borderColor = [UIColor colorWithRed:(11.0/255.0) green:(72.0/255.0) blue:(107.0/255.0) alpha:1.0f].CGColor;
        self.volunteerButton.layer.borderWidth = 0.0f;
        self.volunteerButton.layer.borderColor = [UIColor clearColor].CGColor;
        selectedUser = @"donar";
    }else if (sender == self.volunteerButton)
    {
        self.volunteerButton.layer.borderWidth = 2.0f;
        self.volunteerButton.layer.borderColor = [UIColor colorWithRed:(11.0/255.0) green:(72.0/255.0) blue:(107.0/255.0) alpha:1.0f].CGColor;
        self.donarButton.layer.borderWidth = 0.0f;
        self.donarButton.layer.borderColor = [UIColor clearColor].CGColor;
        selectedUser = @"volunteer";
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
