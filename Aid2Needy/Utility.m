//
//  Utility.m
//  Aid2Needy
//
//  Created by Zenwise Technologies Private Limited on 02/01/19.
//  Copyright © 2019 Zenwise Technologies Private Limited. All rights reserved.
//

#import "Utility.h"

@implementation Utility
- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

+ (id)MF {
    static Utility *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


-(void)GETRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    NSString * urlString;
    NSString * requestName=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:0]];
    if([requestName isEqualToString:@""])
    {
        urlString=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:1]];
    }
    
    
    
    
    //    NSDictionary *headers = @{ @"content-type": @"application/json",
    //                               @"cache-control": @"no-cache",
    //                               @"Accept":@"application/json"
    //
    //                               };
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    //    [request setAllHTTPHeaderFields:headers];
    
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            
                                                            NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                            
                                                            [dict setValue:array forKey:@"data"];
                                                            [dict mutableCopy];
                                                            handler(dict);
                                                        }
                                                    }else
                                                    {
                                                        handler([NSDictionary new]);
                                                    }
                                                    
                                                    
                                                }];
    [dataTask resume];
}


-(void)POSTRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
    NSString * urlString;
    
    NSDictionary *headers;
    NSString * requestName=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:0]];
    headers = @{ @"content-type": @"application/json",
                 @"cache-control": @"no-cache"
                 };
    NSDictionary * params=[[NSDictionary alloc]init];
    urlString = [NSString stringWithFormat:@"%@",[inputArray objectAtIndex:1]];
    if([requestName isEqualToString:@"addfundwatch"])
    {
        params = @{@"action":[inputArray objectAtIndex:2],
                   @"fundids":[inputArray objectAtIndex:3],
                   @"clientid":[inputArray objectAtIndex:4]
                   };
    }
    NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:50.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            if(([httpResponse statusCode]==200)||([httpResponse statusCode]==201))
                                                            {
                                                                NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                [dict setValue:array forKey:@"data"];
                                                                [dict mutableCopy];
                                                                handler(dict);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        handler([NSDictionary new]);
                                                    }
                                                }];
    [dataTask resume];
}

@end
