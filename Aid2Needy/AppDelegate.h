//
//  AppDelegate.h
//  Aid2Needy
//
//  Created by Zenwise Technologies Private Limited on 20/12/18.
//  Copyright © 2018 Zenwise Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

