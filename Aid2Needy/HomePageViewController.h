//
//  HomePageViewController.h
//  Aid2Needy
//
//  Created by Zenwise Technologies Private Limited on 02/01/19.
//  Copyright © 2019 Zenwise Technologies Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomePageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *donarButton;
@property (weak, nonatomic) IBOutlet UIButton *volunteerButton;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end

NS_ASSUME_NONNULL_END
